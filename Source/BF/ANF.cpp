#include "BF.h"

list<u_int> *BF::ANF()
{
    auto fsCopy = new vector<u_int>(*fs);
    TransformMebius();

    auto res = new list<u_int>();
    u_int vars = 0,
              elm;

    int lenOr32 = fs->size() == 1 ? Len() : 32;

    for (auto itr = fs->cbegin(); itr != fs->cend(); itr++)
    {
        elm = *itr;
        for (int j = 0; j < lenOr32; j++)
        {
            if ((elm & 1) == 1)
                res->push_back(vars);
            vars++;
            elm >>= 1;
        }
    }

    delete fs;
    fs = fsCopy;
    return res;
}

string BF::PrettyANF()
{
    list<string> ms;
    auto anf = ANF();
    auto preEnd = --anf->crend();

    for (auto monom = anf->crbegin(); monom != preEnd; ++monom)
        ms.push_back(PrettyANFCore(*monom, varNum));

    auto last = *preEnd == 0 ? "1" : PrettyANFCore(*preEnd, varNum);
    ms.push_back(last);

    delete anf;
    return "ANF = " + Intercalate(ms, " ⊕ ") + "\n";
}

string BF::PrettyANFCore(u_int x, int v)
{
    string s = "";
    if (v == 0)
        return s;

    if ((x & 1) == 1)
        s = "x" + to_string(v);

    return PrettyANFCore(x >> 1, --v) + s;
}