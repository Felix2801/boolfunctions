#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "BF.h"
#include "Timer.h"

using namespace std;

void Test1(int);
void Test2();
void Test3(int);

int main(const int argc, const char *argv[])
{
    if (argc == 1)
        return EXIT_FAILURE;

    int num = atoi(argv[1]);
    int bigNum = atoi(argv[2]);

    Test1(num);
    Test2();
    Test3(bigNum);
}

void Test1(int num)
{
    cout << "  Test 1" << endl;
    BF one(num, ones);
    int cor1 = one.Cor();

    cout << "Number of variables: " << num << endl;

    if (cor1 != num)
        cout << "Failed: 1 -- Function constant '1'" << endl;
    cout << "Cor = " << cor1 << endl
         << endl;
}

void Test2()
{
    cout << "  Test 2" << endl;
    BF two("01101001");
    int Cor = two.Cor();

    cout << "Number of variables: 3" << endl;

    if (Cor != 2)
        cout << "Failed: 2 -- Function \"01101001\"" << endl;
    cout << "Cor = " << Cor << endl
         << endl;
}

void Test3(int bigNum)
{
    cout << "  Test 3" << endl;

    Timer t;
    BF three(bigNum, ones);

    cout << "Number of variables: " << bigNum << endl;

    t.Start();
    int cor3 = three.Cor();
    cout << "Timer = " << t.Finish() << " sec " << endl;

    if (cor3 != bigNum)
        cout << "Failed: 3 -- Function of "
             << to_string(bigNum)
             << " variables." << endl;
    cout << "Cor = " << cor3 << endl
         << endl;
}
