#include "BF.h"

BF::BF(string str)
{
    char s;
    fs = new vector<u_int>(0);
    u_int last = 32,
              elm,
              sBin,
              len;

    for (auto itr = str.cbegin(); itr != str.cend();)
    {
        elm = 0;
        for (int i = 0; i < 32; i++)
        {
            if (itr == str.cend())
            {
                elm >>= 32 - i;
                last = i;
                break;
            }
            s = *itr;

            if (s == '1')
                sBin = 0x80000000;
            else if (s == '0')
                sBin = 0;
            else
            {
                cout << "Expected '0' or '1' !" << endl;
                fs->clear();
                varsOr5 = varNum = 0;
                return;
            }

            elm >>= 1;
            elm |= sBin;
            itr++;
        }
        fs->push_back(elm);

        if (fs->size() >= 0x8000000)
        {
            cout << "Input string is too large !" << endl;
            fs->clear();

            varsOr5 = varNum = 0;
            return;
        }
    }

    len = 32 * (fs->size() - 1) + last;

    if ((len & (len - 1)) != 0)
    {
        cout << "Expected length of string multiple of 32 !" << endl;
        fs->clear();

        varsOr5 = varNum = 0;
        return;
    }

    varNum = CountVariableNum(len);
    varsOr5 = varNum > 5 ? 5 : varNum;
}