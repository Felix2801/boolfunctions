#include "BF.h"

void PrintV(vector<int> *xs)
{
    cout << "Vector: ";
    for (auto x : *xs)
        cout << x << " ";
    cout << endl;
}

void PrintV(vector<long> *xs)
{
    cout << "Vector: ";
    for (auto x : *xs)
        cout << x << " ";
    cout << endl;
}

vector<int32_t> *BF::AutoCor()
{
    vector<int32_t> *wat = WA_Transform();
    // cout << "wat" << endl;
    // PrintV(wat);
    vector<int64_t> *squareWat = DoSquare(wat);
    // cout << "squareWat" << endl;
    // PrintV(squareWat);
    vector<int64_t> *greenScheme = GreenScheme(squareWat, varNum);
    // cout << "greenScheme" << endl;
    // PrintV(greenScheme);
    vector<int32_t> *divTwo = DivTwo(greenScheme, varNum);
    // cout << "divTwo" << endl;
    // PrintV(divTwo);

    delete wat;
    delete greenScheme;
    return divTwo;
}

vector<int64_t> *DoSquare(vector<int32_t> *xs)
{
    auto res = new vector<int64_t>(xs->size());
    auto itrX = xs->cbegin();
    auto itrR = res->begin();

    while (itrX != xs->cend())
    {
        *itrR = (int64_t)(*itrX) * *itrX;
        itrX++;
        itrR++;
    }

    return res;
}

vector<int32_t> *DivTwo(vector<int64_t> *xs, int32_t n)
{
    auto res = new vector<int32_t>(xs->size());
    auto itrX = xs->cbegin();
    auto itrR = res->begin();

    while (itrX != xs->cend())
    {
        *itrR = *itrX >> n;
        itrX++;
        itrR++;
    }

    return res;
}
