#include "BF.h"

vector<int>::const_iterator GetAbsMax(vector<int> *vs)
{
    auto absMax = vs->cbegin();

    for (auto itr = vs->cbegin(); itr != vs->cend(); itr++)
        if (abs(*itr) > abs(*absMax))
            absMax = itr;

    return absMax;
}