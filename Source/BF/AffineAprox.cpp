#include "BF.h"

string BF::AffineAprox()
{
    auto wat = WA_Transform();
    auto absMax = GetAbsMax(wat);
    u_int b = distance(wat->cbegin(), absMax);
    list<string> res;

    for (int i = varNum; i > 0; i--)
    {
        if ((b & 1) != 0)
            res.push_front("x" + to_string(i));
        b >>= 1;
    }

    if (*absMax < 0)
        res.push_back("1");

    delete wat;
    return res.empty() ? "0" : Intercalate(res, " ⊕ ");
}
