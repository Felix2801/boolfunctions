#pragma once
#include <iostream>
#include <ctime>
#include <string>
#include <chrono>

using namespace std;

class Timer
{
private:
    time_t startTime;

public:
    Timer();
    ~Timer();

    void Start();
    double Finish();
};
