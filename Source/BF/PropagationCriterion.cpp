#include "BF.h"

u_int BF::PCmax()
{
    auto autoCor = AutoCor();
    u_int pc;

    for (pc = 1; pc <= varNum; pc++)
        if (not CheckPC(pc, autoCor))
            break;

    delete autoCor;
    return pc - 1;
}

bool BF::CheckPC(u_int pc, vector<int32_t> *autoCor)
{
    u_int mask = (u_1 << pc) - u_1,
          a = mask << (varNum - pc),
          b, c;

    if (autoCor->at(mask) != 0)
        return false;

    while (a != mask)
    {
        if (autoCor->at(a) != 0)
            return false;

        b = (a + 1) & a;
        c = Weight32((b - 1) xor a) - 2;
        a = (((((a + 1) xor a) << 1) + 1) << c) xor b;
    }

    return true;
}