#include "BF.h"

u_int BF::Cor()
{
    auto wats = WA_Transform();
    u_int cor;

    for (cor = 1; cor <= varNum; cor++)
        if (not CorCheck(cor, wats))
            break;

    delete wats;
    return cor - 1;
}

bool BF::CorCheck(u_int cor, vector<int> *wats)
{
    u_int mask = (u_1 << cor) - u_1,
          a = mask << (varNum - cor),
          b, c;

    if (wats->at(mask) != 0)
        return false;

    while (a != mask)
    {
        if (wats->at(a) != 0)
            return false;

        b = (a + 1) & a;
        c = Weight32((b - 1) xor a) - 2;
        a = (((((a + 1) xor a) << 1) + 1) << c) xor b;
    }

    return true;
}