#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "BF.h"
#include "Timer.h"

using namespace std;
void Test1(int);
void Test2();
void Test3(int);

int main(const int argc, const char *argv[])
{
    if (argc == 1)
        return EXIT_FAILURE;
    int vars = atoi(argv[1]);

    Test1(vars);
    Test2();
    Test3(27);
}

void Test1(int vars)
{
    BF one(vars, ones);
    auto result = one.WA_Transform();
    auto frontWA = result->front();

    if (vars == 0 and frontWA == 1)
        exit(0);

    if (frontWA == (u_1 << vars))
    {
        cout << "Failed: In Test1. Should be like: 2^n 0 0 0 0 ..." << endl;
        exit(1);
    }
    delete result;
}

void Test2()
{
    BF one("01101001");
    auto result = one.WA_Transform();
    vector<int32_t> trueAnswer = {0, 0, 0, 0, 0, 0, 0, 8};
    if (*result != trueAnswer)
        cout << "Failed: In Test2. For 01101001 expected 00000008" << endl;
    delete result;
}

void Test3(int bigNum)
{
    cout << "  Test 3" << endl;

    Timer t;
    BF three(bigNum, ones);

    cout << "Number of variables: " << bigNum << endl;

    t.Start();
    auto res = three.WA_Transform();
    cout << "Timer = " << t.Finish() << " sec " << endl;
    delete res;
}