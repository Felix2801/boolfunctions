#include "BF.h"

u_int BF::Len()
{
    return varNum ? (u_int)1 << varNum : 0;
}

u_int BF::Weight()
{
    u_int res = 0;
    for (auto itr : *fs)
        res += Weight32(itr);

    return res;
}

int BF::FuncDegree()
{
    auto anf = ANF();
    int max = 0, x;
    for (auto monom = anf->crbegin(); monom != anf->crend(); ++monom)
    {
        x = Weight32(*monom);
        if (max < x)
            max = x;
    }
    delete anf;
    return max;
}