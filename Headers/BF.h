#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

#include "Support.h"

using namespace std;

enum initCase
{
    zeros,
    ones,
    rands
};

class BF
{

private:
    // число переменных
    u_int varNum;

    // указатель на массив
    vector<u_int> *fs;

    u_int varsOr5;

public:
    // Constructors
    BF();
    BF(int, initCase);
    BF(const BF &);
    ~BF();

    // Constructor from string
    BF(string);

    // Basics
    friend ostream &operator<<(ostream &, BF &);
    BF &operator=(const BF &);
    bool operator==(BF &);

    // Properties
    u_int Len();
    u_int Weight();
    int FuncDegree();

    // Mebuis Transform
    BF *TransformMebius();

    // Algebraic Normal Form
    list<u_int> *ANF();
    /*Выдаёт Полином Жегалкина в читабельном виде*/
    string PrettyANF();

    // Walsh Adamard Transform
    vector<int> *WA_Transform();

    // Correlative immunity
    u_int Cor();

    // Non-Linearity
    u_int NonLinearity();

    // Affine Aproximation
    string AffineAprox();

    // Auto Correlation
    vector<int> *AutoCor();

    // Complete Non-Linearity
    int CN();

    // Propagation Criterion
    u_int PCmax();

private:
    // Algebraic Normal Form
    string PrettyANFCore(u_int, int);

    // Mebuis Transform
    void TransformMebiusCore(vector<u_int>::iterator, int);

    /*Преобразование Мёбиуса для одного 32-битного элемента вектора */
    u_int TransformMebius32(u_int);

    // Walsh Adamard Transform
    /*Возвращает вектор, но каждый бит считается отдельным элементом
    Так же переводит '0' в '1', а '1' в '-1'
    (для последующего преобразования  Уолша-Адамара)*/
    vector<int> *FromBits();

    // Correlative immunity
    bool CorCheck(u_int, vector<int> *);

    // Propagation Criterion
    bool CheckPC(u_int, vector<int32_t> *);
};

// Auto Correlation
vector<long int> *DoSquare(vector<int> *);
vector<int> *DivTwo(vector<long int> *, int);

// Green's Scheme
vector<int> *GreenScheme(vector<int> *, int);
vector<long> *GreenScheme(vector<long int> *, int);

// Support
vector<int>::const_iterator GetAbsMax(vector<int> *);
vector<long>::const_iterator GetAbsMax(vector<long> *);
