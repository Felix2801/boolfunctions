#include "BF.h"

vector<int> *GreenScheme(vector<int> *one, int n)
{
    vector<int>::iterator oneBegin = one->begin(),
                          oneEnd = one->end(),
                          oneItr,
                          itr,
                          midItr,
                          midStop;
    u_int step, mid;
    int buf;

    for (int i = 0; i < n; i++)
    {
        step = u_1 << (i + 1),
        mid = step >> 1;

        for (oneItr = oneBegin; oneItr != oneEnd; oneItr += step)
        {
            itr = oneItr;
            midStop = midItr = itr + mid;
            while (itr != midStop)
            {
                buf = *itr;
                *itr = *itr + *midItr;
                *midItr = buf - *midItr;
                itr++;
                midItr++;
            }
        }
    }
    return one;
}

vector<int64_t> *GreenScheme(vector<int64_t> *one, int n)
{
    vector<int64_t>::iterator oneBegin = one->begin(),
                           oneEnd = one->end(),
                           oneItr,
                           itr,
                           midItr,
                           midStop;
    u_int step, mid;
    int64_t buf;

    for (int i = 0; i < n; i++)
    {
        step = u_1 << (i + 1),
        mid = step >> 1;

        for (oneItr = oneBegin; oneItr != oneEnd; oneItr += step)
        {
            itr = oneItr;
            midStop = midItr = itr + mid;
            while (itr != midStop)
            {
                buf = *itr;
                *itr = *itr + *midItr;
                *midItr = buf - *midItr;
                itr++;
                midItr++;
            }
        }
    }
    return one;
}