#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <list>

#define u_1 (u_int)1

using namespace std;

/*По длине входной строки определяет сколько переменных в булевой функции
Не проверяет кратность двойке!*/
int CountVariableNum(int);

/*Считает вес одной целой переменной 32 бита*/
u_int Weight32(u_int);

/*Переводит переменную в строковый вид единиц и нулей
Второй параметр определяет количество символов в выводе*/
string MakeVarsBin(u_int, int);

/*Объединяет лист из строк в одну строку
Между каждым элементом добавляется строка из второго аргумента*/
string Intercalate(list<string>, string);

/*Инициилизирует генератор рандомных значений*/
mt19937 GetRandGen();

