#include "Timer.h"

Timer::Timer() {}

Timer::~Timer() {}

void Timer::Start()
{
     startTime = time(0);
}

double Timer::Finish()
{
     time_t finTime = time(0);
     double diff = difftime(finTime, startTime);
     return diff;
}