#include "BF.h"

ostream &operator<<(ostream &o, BF &one)
{
    string res = "";
    u_int vars = 0,
              elm;

    cout << "Number of variables = "
         << one.varNum << endl;

    int lenOr32 = one.fs->size() == 1 ? one.Len() : 32;

    for (auto itr = one.fs->cbegin(); itr != one.fs->cend(); itr++)
    {
        elm = *itr;
        for (int j = 0; j < lenOr32; j++)
        {
            auto elm1 = (elm & 1);
            res.append(to_string(elm1));
            cout << MakeVarsBin(vars++,one.varNum) << " "
                 << elm1 << endl;
            elm >>= 1;
        }
    }

    cout << "f = " << res << endl;
    return o;
}

BF &BF::operator=(const BF &one)
{
    varNum = one.varNum;
    delete fs;
    fs = new vector<u_int>(*one.fs);
    return *this;
}

bool BF::operator==(BF &one)
{
    if (varNum != one.varNum)
        return false;
    if (*fs != *one.fs)
        return false;
    return true;
}


/*
inline string BF::MakeVarsBin(u_int vs)
{
    return MakeVarsBinQore(vs, 0);
}

string BF::MakeVarsBinQore(u_int vs, int sh)
{
    if (sh == varNum)
        return "";
    string bit = to_string(vs & 1);
    return MakeVarsBinQore(vs >> 1, ++sh).append(bit);
}
*/