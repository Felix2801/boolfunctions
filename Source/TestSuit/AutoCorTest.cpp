// #include "BF.h"
// #include "Timer.h"

// void AutoCorTest();
// void CNTest();
// vector<int> GetUnique(vector<int>);

// int main()
// {
//     // AutoCorTest();
//     // CNTest();
//     auto one = "00111010";
//     auto two = "1110";
//     BF bf(one);
//     // for (auto i : *bf.AutoCor())
//     //     cout << i << " ";
//     cout << endl
//          << "for " << one << endl
//          << "CN = " << bf.CN() << endl;
// }

// void AutoCorTest()
// {
//     cout << "\tAuto cor test" << endl;
//     BF bf(26, ones);
//     Timer timer;
//     vector<int> *autoCor;
//     double finishTime;

//     timer.Start();
//     autoCor = bf.AutoCor();
//     finishTime = timer.Finish();

//     cout << "Timer: " << finishTime << endl
//          << "Vector: ";
//     for (auto itr : GetUnique(*autoCor))
//         cout << itr << " ";
//     cout << endl
//          << endl;

//     delete autoCor;
// }

// void CNTest()
// {
//     cout << "\tCN test" << endl;
//     BF *bf;
//     int cn;
//     string test1 = "0001000100011110",
//            test2 = "0001000100011110000100010001111000010001000111101110111011100001";

//     bf = new BF(test1);
//     cout << "Test for " << test1 << endl
//          << "CN = " << bf->CN() << endl
//          << endl;
//     delete bf;

//     bf = new BF(test2);
//     cout << "Test for " << test2 << endl
//          << "CN = " << bf->CN() << endl
//          << endl;
//     delete bf;
// }

// vector<int> GetUnique(vector<int> xs)
// {
//     vector<int> res;
//     int buf = -1;
//     for (auto itr : xs)
//     {
//         if (buf != itr)
//         {
//             res.push_back(itr);
//             buf = itr;
//         }
//     }
//     return res;
// }
