#include "BF.h"

u_int masks[5] = {0b10101010101010101010101010101010,
                  0b11001100110011001100110011001100,
                  0b11110000111100001111000011110000,
                  0b11111111000000001111111100000000,
                  0b11111111111111110000000000000000};

BF *BF::TransformMebius()
{
    auto sh = varNum >= 5 ? varNum - 5 : 0;
    auto n = (u_int)1 << sh;
    TransformMebiusCore(fs->begin(), n);
    return this;
}

void BF::TransformMebiusCore(vector<u_int>::iterator itr, int n)
{
    if (n == 1)
    {
        *itr = TransformMebius32(*itr);
        return;
    }

    int m = n >> 1;
    auto midItr = itr + m;
    TransformMebiusCore(itr, m);
    TransformMebiusCore(midItr, m);

    for (int i = 0; i < m; i++)
        *(midItr + i) ^= *(itr + i);
}

u_int BF::TransformMebius32(u_int x)
{
    int c = varsOr5;
    u_int sh = 1, m;

    for (int i = 0; i < c; i++)
    {
        m = masks[i];
        x ^= (x << sh) & m;
        sh <<= 1;
    }
    return x;
}

/*  Реализация без рекурсии

BF *BF::TransformMebius2()
{
    auto fsEnd = fs->end();

    for (auto itr = fs->begin(); itr != fsEnd; itr++)
    {
        *itr = TransformMebiusSolo(*itr);
    }

    for (int i = 0; i < varNum - 5; i++)
    {
        u_int step = (u_int)1 << (i + 1);
        u_int mid = step >> 1;
        vector<u_int>::iterator midItr;
        
        for (auto itr = fs->begin(); itr != fsEnd; itr += step)
        {
            midItr = itr + mid;
            for (u_int j = 0; j < mid; j++)
                *(midItr + j) ^= *(itr + j);
        }
    }

    return this;
}

*/
