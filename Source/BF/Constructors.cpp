#include "BF.h"

BF::BF()
{
    varNum = 0;
    fs = new vector<u_int>(1);
}

BF::BF(int count, initCase initCase)
{
    if (count > 31 || count < 1)
    {
        cout << "Variable num between 1 and 31" << endl;
        varsOr5 = varNum = 0;
        fs = new vector<u_int>(1);
        return;
    }

    varNum = count;
    varsOr5 = varNum > 5 ? 5 : varNum;

    int vl = Len() >> 5;
    fs = new vector<u_int>(vl ? vl : 1);

    u_int fullOne = 0b11111111111111111111111111111111;
    auto gen = GetRandGen();

    switch (initCase)
    {
    case zeros:
        return;

    case ones:
        for (auto x = fs->begin(); x != fs->end(); x++)
            *x = fullOne;
        break;

    case rands:
        for (auto x = fs->begin(); x != fs->end(); x++)
            *x = gen();
        break;

    default:
        break;
    }

    if (varNum < 5)
        *fs->begin() &= fullOne >> (32 - Len());
}

BF::BF(const BF &samp)
{
    varNum = samp.varNum;
    varsOr5 = samp.varNum;
    fs = new vector<u_int>(*samp.fs);
}

BF::~BF()
{
    delete fs;
}