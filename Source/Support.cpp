#include "Support.h"

int CountVariableNum(int x)
{
    if (x == 0)
        return 0;

    int res = 0;
    while ((x & 1) == 0)
    {
        res++;
        x >>= 1;
    }

    return res;
}

u_int Weight32(u_int elm)
{
    u_int res;
    for (res = 0; elm != 0; res++)
        elm &= elm - 1;
    return res;
}

string MakeVarsBin(u_int vs, int sh)
{
    if (sh == 0)
        return "";
    string bit = to_string(vs & 1);
    return MakeVarsBin(vs >> 1, sh - 1).append(bit);
}

string Intercalate(list<string> str, string p)
{
    string res = "";
    auto preEnd = --str.cend();

    for (auto s = str.cbegin(); s != preEnd; s++)
        res += *s + p;

    return res + *preEnd;
}

mt19937 GetRandGen()
{
    random_device rd;
    mt19937 gen(rd());
    return gen;
}