#include "BF.h"

u_int BF::NonLinearity()
{
    u_int deg = u_1 << (varNum - 1);
    auto wat = WA_Transform();
    int absMax = *GetAbsMax(wat);
    delete wat;
    return deg - (abs(absMax) >> 1);
}