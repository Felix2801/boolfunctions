#include "BF.h"

vector<int> *BF::WA_Transform()
{
    auto bits = FromBits();
    return GreenScheme(bits, varNum);
}

vector<int> *BF::FromBits()
{
    auto fsEnd = fs->cend();
    int c = u_1 << varsOr5;

    u_int copy;

    auto res = new vector<int>(u_1 << varNum);
    auto resItr = res->begin();

    for (auto itr = fs->cbegin(); itr != fsEnd; itr++)
    {
        copy = *itr;
        for (int i = 0; i < c; i++)
        {
            *resItr = copy & 1 ? -1 : 1;
            copy >>= 1;
            resItr++;
        }
    }

    return res;
}
