#include "BF.h"

int BF::CN()
{
    if (varNum < 2)
        return 0;

    vector<int> *autoCor = AutoCor();
    *autoCor->begin() = 0;

    int absMax = *GetAbsMax(autoCor),
        twoDegN2 = 1 << (varNum - 2),
        absDiv4 = abs(absMax) >> 2,
        cn = twoDegN2 - absDiv4;

    delete autoCor;
    return cn;
}